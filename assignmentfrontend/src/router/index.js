import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import AllUsers from '@/components/AllUsers'
import AddUser from '@/components/AddUser'
import Header from '@/components/Header'
import Register from '@/components/Register'
import Dashboard from '@/components/Dashboard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      redirect: {
        name: 'login'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'allUsers',
      component: AllUsers
    },
    {
      path: '/add-user',
      name: 'addUser',
      component: AddUser
    },
    {
      path: '/header',
      name: 'header',
      component: Header
    },
    {
      path: '/register/:id',
      name: 'register',
      component: Register
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    }
  ]
})
