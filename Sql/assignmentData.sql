-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 12, 2021 at 07:38 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.2.34-18+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignmentData`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_03_11_082206_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_profile_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_pic_url` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_profile_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_pic_url` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `user_profile_name`, `user_pic_url`, `admin_profile_name`, `admin_pic_url`, `signup_status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', 'user1615533755.png', 'http://127.0.0.1:8000/images/user1615533755.png', 'admin1615533755.png', 'http://127.0.0.1:8000/images/admin1615533755.png', '1', '$2y$10$XYi6bmL6Y88ya8JPXAn2uejMEulzmSX1DI8A0Rr/qXWGvC0.CfeM6', NULL, '2021-03-12 01:09:13', '2021-03-12 02:27:55'),
(3, 'user03', 'test3@gmail.com', 'user1615550558.png', 'http://127.0.0.1:8000/images/user1615550558.png', 'admin1615550558.png', 'http://127.0.0.1:8000/images/admin1615550558.png', '1', '$2y$10$xPzmBn2UoXR72KyBr7HkIuYXS/ERMhO60NAPw3/8C4DT6ANPY251S', NULL, '2021-03-12 02:13:39', '2021-03-12 06:32:38'),
(7, 'palash', 'palash@vkaps.com', 'user1615552768.png', 'http://127.0.0.1:8000/images/user1615552768.png', 'admin1615552768.png', 'http://127.0.0.1:8000/images/admin1615552768.png', '1', '$2y$10$7yiX9z12uLCkuHADMkQox.qhXuumw/p/6Myl.AqDVGwVPrXdsEGpO', NULL, '2021-03-12 07:07:52', '2021-03-12 07:19:37'),
(8, 'Anupam', 'anupam.rawal@vkaps.com', 'user1615555606.png', 'http://127.0.0.1:8000/images/user1615555606.png', 'admin1615555606.png', 'http://127.0.0.1:8000/images/admin1615555606.png', '1', '$2y$10$QP/q6m1C1IrrtEWGFsRTvuN3qg1bjmDAyEHQk91q6F6mgX9mqyYPG', NULL, '2021-03-12 07:20:13', '2021-03-12 07:56:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
