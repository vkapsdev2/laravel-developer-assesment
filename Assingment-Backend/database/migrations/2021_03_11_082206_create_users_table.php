<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name',191)->nullable();
            $table->string('email')->unique();
            $table->string('user_profile_name', 40)->nullable();
            $table->string('user_pic_url', 80)->nullable();
            $table->string('admin_profile_name', 40)->nullable();
            $table->string('admin_pic_url', 80)->nullable();
            $table->string('signup_status')->default(0);
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
