<?php

namespace App\Mail;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function sendMail(Request $request)
    {
        $alldata = $request->json()->all();
        $email = $alldata['email'];

        Mail::raw('Hi, welcome user!', function ($message) use ($email) {
            $message->to($email)->from("futuretechdotcodotin@gmail.com")->subject("test mail");
        });
    }
}
