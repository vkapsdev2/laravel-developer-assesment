<?php

namespace App\Mail;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Intervention\Image\Facades\Image;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result =  User::where('id', '!=', '1')
            ->orderBy('id', 'desc')
            ->get();

        return response()->json(array('status' => "true", 'data' => $result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(array('status' => "false", 'message' => "Wrong email!"));
        }


        $email = $request->input('email');
        $userdata = User::where('email', $email)->first();
        // echo $userdata['id'];
        // dd($userdata);
        
        if ($userdata === null) {
            $user = new User;
            $user->email = $email;
            $user->save();
            $userdata1 = User::where('email', $email)->first();
            // echo $userdata1['id'];
            // dd($userdata1);
            $mail_body = 'http://localhost:8080/register/' . $userdata1['id'];
            Mail::raw($mail_body, function ($message) use ($email) {
                $message->to($email)->from("futuretechdotcodotin@gmail.com")->subject("New Invitation");
            });

            return response()->json(array('status' => "true", 'messsage' => "Invited Succsesfully"));
        } else {
            return response()->json(array('status' => "false", 'messsage' => "Invitation Already sent!"));
        }
    }

    public function saveDetails(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'user_name' => 'required',
            'password' => 'required',

        ]);

        $email = $request->input('email');
        $userName = $request->input('user_name');
        $userdata = User::where('email', $email)->first();
        $nametaken = $request->input('user_name');

        if ($userdata != null) {
            if ($nametaken != "admin") {
                $userdata->update([
                    'user_name' => $request->user_name,
                    'password' => $request->password,
                    'signup_status' => true
                ]);

                $mail_body = 'Hello '.$userName.",\n"."your are registered successfully!";
                Mail::raw($mail_body, function ($message) use ($email) {
                    $message->to($email)->from("futuretechdotcodotin@gmail.com")->subject("Registered Sucessfully!");
                });


                return response()->json(array('status' => "true", 'message' => "Data saved Successfully"));
            } else {
                return response()->json(array('status' => "false", 'message' => "This name can't be taken"));
            }
        } else {
            return response()->json(array('status' => "false", 'message' => "User doesn't exist"));
        }
    }

    public function saveProfilePic(Request $request)
    {
        $request->validate([
            'profile_pic' => 'required',
            'email' => 'required'
        ]);

        $email = $request->input('email');
        $userdata = User::where('email', $email)->first();


        if ($userdata != null) {
            $userdata->update([
                'profile_pic' => $request->profile_pic,
            ]);
            return response()->json(array('status' => "true", 'message' => "Profile Picture updated"));
        } else {
            return response()->json(array('status' => "false", 'message' => "User doesn't exist"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userExist = User::find($id);

        if ($userExist !== null) {
            $userExist->destroy($id);
            return response()->json(array('status' => "true", 'message' => "User deleted Successfully"));
        } else {
            return response()->json(array('status' => "false", 'message' => "Not Exist"));
        }

        // dd($userdata);
    }
    public function loginUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        // $validatedData= $request->validate([
        //     'email' => 'required|email'
        // ]);

        if ($validator->fails()) {
            return response()->json(array('status' => "false", 'message' => "Wrong email!"));
        }


        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $email = $request->input('email');
            $userdata = User::where('email', $email)->first();

            if ($userdata['user_name'] == "admin") {
                return response()->json(array('status' => "true", 'message' => "login Successfully", "user_id" => $userdata['id'], "user_token" => encrypt($userdata['id']), "role" => "admin"));
            } else {
                return response()->json(array('status' => "true", 'message' => "login Successfully", "user_id" => $userdata['id'], "user_token" => encrypt($userdata['id']), "role" => "user"));
            }
        } else {
            return response()->json(array('status' => "false", 'message' => "Wrong Credentials!"));
        }
    }

    public function logoutUser(Request $request)
    {
        Auth::logout();
        return response()->json(array('status' => "true", 'message' => "logged out successfully"));
    }

    public function user_info($id)
    {
        $userExist = User::find($id);

        if ($userExist !== null) {
            // dd($userExist);
            return response()->json(array('status' => "true", "email" => $userExist['email'], "user_pic_url" => $userExist['user_pic_url']));
        } else {
            return response()->json(array('status' => "false", 'message' => "User Doesn't Exist"));
        }
    }

    public function imageUploadPost(Request $request, $id)
    {
        $userExist = User::find($id);

        if ($userExist !== null) {

            $userdata = User::where('id', $id)->first();

            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $adminImageName = 'admin' . time() . '.' . $request->image->extension();
            $userImageName = 'user' . time() . '.' . $request->image->extension();

            // $request->image->move(public_path('images'), $userImageName);
            // $request->image->move(public_path('images'), $adminImageName);

            $image = $request->image;
            // $input['imagename'] = time() . '.' . $image->extension();

            $destinationPath = public_path('/images');
            $img = Image::make($image->path());
            $img->resize(54, 54, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $userImageName);

            $img->resize(256, 256, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $adminImageName);


            // $image->move($destinationPath, $input['imagename']);

            $userdata->update([
                'user_profile_name' => $userImageName,
                'user_pic_url' => "http://127.0.0.1:8000/images/" . $userImageName,
                'admin_profile_name' => $adminImageName,
                'admin_pic_url' => "http://127.0.0.1:8000/images/" . $adminImageName
            ]);

            // return back()
            //     ->with('success', 'You have successfully upload image.')
            //     ->with('image', $imageName);
            return response()->json(array('status' => "true", 'message' => "Success", 'userImageName' => $userImageName, 'userImageUrl' => "http://127.0.0.1:8000/images/" . $userImageName, 'adminImageName' => $adminImageName, 'adminImageUrl' => "http://127.0.0.1:8000/images/" . $adminImageName));
        } else {
            return response()->json(array('status' => "false", 'message' => "User Doesn't Exist"));
        }
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     //
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }
}
