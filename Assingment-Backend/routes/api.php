<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/test', 'TestController@indefgdsgshx');

Route::get('/logout', 'UserController@logoutUser');

Route::get('/list', 'UserController@index');

Route::post('/inviteUser', 'UserController@storeMail');

Route::post('/signUp', 'UserController@saveDetails');

Route::post('/savePic', 'UserController@saveProfilePic');

Route::post('/login', 'UserController@loginUser');

Route::delete('/delete/{id}', 'UserController@destroy');

Route::get('/user/info/{id}', 'UserController@user_info');

Route::post('/upload/image/{id}', 'UserController@imageUploadPost');






